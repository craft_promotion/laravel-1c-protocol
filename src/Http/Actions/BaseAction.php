<?php


namespace Mavsan\LaProtocol\Http\Actions;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mavsan\LaProtocol\Model\FileName;

class BaseAction extends Controller
{

    protected $request;
    protected $mode;
    protected $stepCheckAuth = 'checkauth';
    protected $stepInit = 'init';
    protected $stepFile = 'file';
    protected $stepImport = 'import';
    protected $stepQuery = 'query';
    protected $stepComplete = 'complete';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->mode = $request->get('mode');
    }

    public function run() {

        if (!$this->userLogin()) {
            return $this->failure('Wrong username or password');
        }

        switch ($this->mode) {
            case $this->stepCheckAuth:
                return $this->checkAuth();
                break;
            case $this->stepInit:
                return $this->init();
                break;
        }
    }

    public function __toString()
    {
        return $this->run();
    }

    /**
     * Сообщение о ошибке
     *
     * @param string $details - детали, строки должны быть разделены /n
     *
     * @return string
     */
    protected function failure($details = "")
    {
        $return = "failure" . (empty($details)? "": "\n$details");
        return $this->answer($return);
    }

    /**
     * Ответ серверу
     *
     * @param $answer
     *
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function answer($answer)
    {
        return response(mb_convert_encoding($answer, 'windows-1251'));
    }

    protected function answerXML($answer){
        return $this->answer($answer)->withHeaders([
            'Content-Type' => 'application/xml; charset=windows-1251'
        ]);
    }

    /**
     * Попытка входа
     * @return bool
     */
    protected function userLogin()
    {

        if (!Auth::check()) {
            $user = \Request::getUser();
            $pass = \Request::getPassword();

            $attempt = Auth::attempt(['email' => $user, 'password' => $pass]);

            if (!$attempt) {
                return false;
            }

            $gates = config('protocolExchange1C.gates', []);
            if (! is_array($gates)) {
                $gates = [$gates];
            }

            foreach ($gates as $gate) {
                if (\Gate::has($gate) && \Gate::denies($gate, Auth::user())) {
                    Auth::logout();
                    return false;
                }
            }

            return true;
        }

        return true;
    }

    /**
     * Авторизация 1с в системе
     */
    protected function checkAuth()
    {
        $cookieName = config('session.cookie');
        $cookieID = session()->getId();

        return $this->answer("success\n$cookieName\n$cookieID");
    }

    /**
     * Инициализация соединения
     * @return string
     */
    protected function init()
    {
        $zip = "zip=".($this->canUseZip() ? 'yes' : 'no');

        return $this->answer("$zip\nfile_limit="
            .config('protocolExchange1C.maxFileSize'));
    }

    /**
     * Можно ли использовать ZIP
     * @return bool
     */
    protected function canUseZip()
    {
        return function_exists("zip_open");
    }

    /**
     * Отправка ответа, что все в порядке
     * @return string
     */
    protected function success()
    {
        return $this->answer('success');
    }

    /**
     * Распаковка файлов, если требуется
     *
     * @return string
     */
    protected function unzipIfNeed()
    {
        $files = session('inputZipped', []);

        if (empty($files)) {
            return '';
        }

        $file = array_shift($files);

        session(['inputZipped' => $files]);

        /** @var \Madnest\Madzipper\Madzipper $zip */
        try {
            $zip = \Madzipper::make($file);
            if ($zip->getStatus() === false) {
                return 'Error opening zipped: '.$file;
            }
        } catch (Exception $e) {
            return 'Error opening zipped: '.$e->getMessage();
        }

        $path =
            config('protocolExchange1C.inputPath').'/'.$this->checkInputPath();

        $zip->extractTo($path);

        \File::delete($file);

        return 'more';
    }

    /**
     * Получение и очистка имени файла. Все, что тут сделано - взято из 1С
     * Битрикс
     *
     * В случае, если имя переданное файла не проходит фильтры - будет
     * возвращена пустая строка
     *
     * @param string $fileName
     *
     * @return string
     */
    protected function importGetFileName($fileName)
    {
        $modeFileName = new FileName($fileName);
        if ($modeFileName->hasScriptExtension()
            || $modeFileName->isFileUnsafe()
            || ! $modeFileName->validatePathString("/$fileName")
        ) {
            return '';
        }

        return $modeFileName->getFileName();
    }

}
