<?php


namespace Mavsan\LaProtocol\Http\Actions;


use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Mavsan\LaProtocol\Interfaces\Import;
use Mavsan\LaProtocol\Interfaces\Sale;

class SaleIn extends BaseAction
{

    public function run()
    {
        $parent = parent::run();
        if ($parent)
            return $parent;

        switch ($this->mode) {
            case $this->stepFile:
                return $this->success();
                break;
            case $this->stepQuery:
                return $this->query();
                break;
            default:
                return $this->failure("Wrong step - " . $this->mode);
        }
    }

    private function query() {

        $modelCLass = config('protocolExchange1C.saleWorkModel');
        // проверка модели
        if (empty($modelCLass)) {
            return $this->failure('Mode: '.$this->stepImport
                .', please set model to import data in catalogWorkModel key.');
        }

        /** @var Import $model */
        $model = \App::make($modelCLass);
        if (! $model instanceof Sale) {
            return $this->failure('Mode: '.$this->stepImport.' model '
                .$modelCLass
                .' must implement \Mavsan\LaProtocol\Interfaces\Import');
        }

        $data = call_user_func(array($model, 'getData'));

        $view = property_exists($model, 'view') && View::exists($model->view)? $model->view: 'protocol_1c::sale';

        return  $this->answerXML(View::make($view, compact('data'))->render());
    }

}
