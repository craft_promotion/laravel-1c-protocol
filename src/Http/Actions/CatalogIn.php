<?php


namespace Mavsan\LaProtocol\Http\Actions;

use Mavsan\LaProtocol\Interfaces\Import;
use Mavsan\LaProtocol\Model\FileName;

class CatalogIn extends BaseAction
{

    public function run()
    {

        $parent = parent::run();
        if ($parent instanceof \Illuminate\Http\Response || $parent instanceof \Illuminate\Contracts\Routing\ResponseFactory) {
            return $parent;
        }

        switch ($this->mode) {
            case $this->stepFile:
                return $this->getFile();
                break;
            case $this->stepImport:
                return $this->import();
                break;
            case $this->stepComplete:
                return true;
                break;
            default:
                return $this->failure("Wrong step - " . $this->mode);
        }
    }

    /**
     * Импорт данных
     * @return string
     */
    private function import() {
        $unZip = $this->unzipIfNeed();

        if ($unZip == 'more') {
            return $this->answer('progress');
        } elseif (! empty($unZip)) {
            return $this->failure('Mode: '.$this->stepImport.' '.$unZip);
        }

        // проверка валидности имени файла
        $fileName =
            $this->importGetFileName($this->request->get('filename'));
        if (empty($fileName)) {
            return $this->failure('Mode: '.$this->stepImport
                .' wrong file name: '
                .$this->request->get('filename'));
        }

        $modelCLass = config('protocolExchange1C.catalogWorkModel');
        // проверка модели
        if (empty($modelCLass)) {
            return $this->failure('Mode: '.$this->stepImport
                .', please set model to import data in catalogWorkModel key.');
        }

        /** @var Import $model */
        $model = \App::make($modelCLass);
        if (! $model instanceof Import) {
            return $this->failure('Mode: '.$this->stepImport.' model '
                .$modelCLass
                .' must implement \Mavsan\LaProtocol\Interfaces\Import');
        }

        try {
            $fullPath = $this->getFullPathToFile($fileName);

            if (! \File::isFile($fullPath)) {
                return $this->failure('Mode: '.$this->stepImport.', file '
                    .$fullPath
                    .' not exists');
            }

            $ret = $model->import($fullPath);

            $retData = explode("\n", $ret);
            $valid = [
                Import::answerSuccess,
                Import::answerProgress,
                Import::answerFailure,
            ];

            if (! in_array($retData[0], $valid)) {
                return $this->failure('Mode: '.$this->stepImport.' model '
                    .$modelCLass
                    .' model return wrong answer');
            }

            $log = $model->getAnswerDetail();

            return $ret."\n".$log;

        } catch (\Exception $e) {
            return $this->failure('Mode: '.$this->stepImport
                .", exception: {$e->getMessage()}\n"
                ."{$e->getFile()}, {$e->getLine()}\n"
                ."{$e->getTraceAsString()}");
        }
    }

    /**
     * Получение файла(ов)
     * @return string
     */
    protected function getFile()
    {
        $modelFileName = new FileName($this->request->input('filename'));
        $fileName = $modelFileName->getFileName();

        if (empty($fileName)) {
            return $this->failure('Mode: '.$this->stepFile
                .', parameter filename is empty');
        }

        $fullPath = $this->getFullPathToFile($fileName, true);
		/* Тип файла */
        $ext = pathinfo($fullPath, PATHINFO_EXTENSION);

        $fData = $this->getFileGetData();

        if (empty($fData)) {
            return $this->failure('Mode: '.$this->stepFile
                .', input data is empty.');
        }

        if ($file = fopen($fullPath, 'ab')) {

            $dataLen = mb_strlen($fData, 'latin1');
            $result = fwrite($file, $fData);


            if ($result === $dataLen && $ext == "zip") {
                // файлы, требующие распаковки
                $files = [];

                if ($this->canUseZip()) {
                    $files = session('inputZipped', []);
                    $files[$fileName] = $fullPath;
                }

                session(['inputZipped' => $files]);

                return $this->success();
            }

            $this->failure('Mode: '.$this->stepFile
                .', can`t wrote data to file: '.$fullPath);

        } else {
            return $this->failure('Mode: '.$this->stepFile.', cant open file: '
                .$fullPath.' to write.');
        }

        return $this->failure('Mode: '.$this->stepFile.', unexpected error.');
    }

    /**
     * Формирование полного пути к файлу
     *
     * @param string $fileName
     *
     * @param bool   $clearOld
     *
     * @return string
     */
    protected function getFullPathToFile($fileName, $clearOld = false)
    {
        $workDirName = $this->checkInputPath();

        if ($clearOld) {
            $this->clearInputPath($workDirName);
        }

        $path = config('protocolExchange1C.inputPath');

        return $path.'/'.$workDirName.'/'.$fileName;
    }

    /**
     * Формирование имени папки, куда будут сохранятся принимаемые файлы
     * @return string
     */
    protected function checkInputPath()
    {

        $inputPath = config('protocolExchange1C.inputPath');

        $folderName = session('inputFolderName');

        if (!empty($folderName) && !\File::isDirectory($inputPath . '/' . $folderName))
            \File::makeDirectory($inputPath . '/' . $folderName, 0755, true. true);


        if (empty($folderName)) {
            $folderName = date('Y-m-d_H:i:s').'_'.time();

            $fullPath = config('protocolExchange1C.inputPath').'/'.$folderName;

            if (! \File::isDirectory($fullPath)) {
                \File::makeDirectory($fullPath, 0755, true. true);
            }

            session(['inputFolderName' => $folderName]);
        }

        return $folderName;
    }

    /**
     * Очистка папки, где хранятся входящие файлы от предыдущих принятых файлов
     *
     * @param $currentFolder
     */
    protected function clearInputPath($currentFolder)
    {
        $storePath = config('protocolExchange1C.inputPath');

        foreach (\File::directories($storePath) as $path) {
            if (\File::basename($path) != $currentFolder) {
                \File::deleteDirectory($path);
            }
        }
    }

    /**
     * получение контента файла
     *
     * @return string
     */
    protected function getFileGetData()
    {
        return \Request::getContent();
    }

}
