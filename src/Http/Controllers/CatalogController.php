<?php
/**
 * ProtocolController.php
 * Date: 16.05.2017
 * Time: 16:09
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaProtocol\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Mavsan\LaProtocol\Http\Actions\CatalogIn;
use Mavsan\LaProtocol\Http\Actions\SaleIn;

class CatalogController extends BaseController {

    public static $types = [
        'catalog' => CatalogIn::class,
        'sale' => SaleIn::class,
    ];

    public function __construct()
    {
        $this->middleware(\Illuminate\Session\Middleware\StartSession::class);
        $this->middleware(\App\Http\Middleware\EncryptCookies::class);
        $this->middleware(\Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class);
    }

    public function index(Request $request) {

        $type = $request->get('type');
		
		if (empty($type))
                return iconv('UTF-8', 'windows-1251', "failure\nEmpty type parameter - ");

        if (config('protocolExchange1C.logCommandsOf1C', false)) {
            $mode = $request->get('mode');
            
            \Log::debug('Command from 1C type: '.$type.'; mode: '.$mode);
        }

        if (isset(static::$types[$type])) {
            return app()->make(static::$types[$type])->callAction('run', []);
        }

        return iconv('UTF-8', 'windows-1251', "failure\nUnknown type - " . $type);

    }

}
