<?php
/**
 * Import.php
 * Date: 18.05.2017
 * Time: 16:46
 * Author: Maksim Klimenko
 * Email: mavsan@gmail.com
 */

namespace Mavsan\LaProtocol\Interfaces;


interface Sale
{
    public function getData();
}
